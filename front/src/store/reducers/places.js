import {PLACES_SUCCESS} from "../actions/actionTypes";

const initialState = {
    places: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case PLACES_SUCCESS:
            return {...state, places: action.place};

        default:
            return state;
    }
};

export default reducer;