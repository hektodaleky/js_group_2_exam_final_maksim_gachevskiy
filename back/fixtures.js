const mongoose = require('mongoose');
const config = require('./config');


const User = require('./models/User');
const Place = require('./models/Place');
const Photo = require('./models/Photo');


mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;


db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('places');
        await db.dropCollection('photos');

    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }


    const [admin, user] = await User.create({
        password: 123,
        username: "TestAdmin",
        role: 'admin'
    }, {
        password: 123,
        username: "TestUser",
        role: 'user'
    });
    const [cafe] = await Place.create({
        title: "Cafe Number One",
        description: "The best of the best",
        created: user._id,
        photo: 'k1.jpeg'
    });
    const [first, second, third] = await Photo.create({
            user: user._id,
            place: cafe._id,
            name: 'k2.jpg'
        },
        {
            user: user._id,
            place: cafe._id,
            name: 'k3.jpeg'
        },
        {
            user: user._id,
            place: cafe._id,
            name: 'k4.jpeg'
        });


    db.close();
});