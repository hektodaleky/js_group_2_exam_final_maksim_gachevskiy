import React, {Fragment} from "react";
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";


const Toolbar = ({user, logout}) => {
    return (
        <Navbar>
            <Navbar.Header>
                <Navbar.Brand>
                    <LinkContainer to="/">
                        <a>Рейтинг заведений</a>
                    </LinkContainer>
                </Navbar.Brand>
                <Navbar.Toggle/>
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav pullRight>
                    {user ?
                        <Fragment>
                            <h4>{`Привет  ${user.username}`}</h4>
                        <NavItem onClick={() => logout()}>Выйти</NavItem>
                        </Fragment>
                        :
                        <Fragment>
                            <LinkContainer to="/login" exact>
                                <NavItem>Войти</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/register" exact>
                                <NavItem>Регистрация</NavItem>
                            </LinkContainer>
                        </Fragment>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};

export default Toolbar;