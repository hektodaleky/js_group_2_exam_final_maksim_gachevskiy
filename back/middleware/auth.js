const User = require('../models/User');

const auth = async (req, res, next) => {
    const token = req.get('Token');

    if (!token) {
        return res.status(401).send({message: 'У вас нет токена для аутентификации'});
    }

    const user = await User.findOne({token});
    if (!user) {
        return res.status(401).send({message: 'Такой пользователь не найден в базе данных'});
    }

    req.user = user;

    next();
};

module.exports = auth;