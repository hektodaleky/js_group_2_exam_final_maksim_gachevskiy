import React, {Component, Fragment} from "react";
import {NotificationContainer} from "react-notifications";
import "react-notifications/lib/notifications.css";
import {connect} from "react-redux";

import {logoutUser} from "../../store/actions/users";
import Toolbar from "../../componets/UI/Toolbar/Toolbar";

class Layout extends Component {
    render() {
        return (
            <Fragment>

                    <NotificationContainer/>
                    <header>
                        <Toolbar user={this.props.user} logout={this.props.logoutUser}/>
                    </header>
                    <main className="container">{this.props.children}</main>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
