import React from 'react'
import {Button, Jumbotron} from "react-bootstrap";
import config from '../../config'

const OnePlace = ({title, food, interior, service, count,photo,click}) => {
    return (
        <div style={{width: '18%'}}><Jumbotron>
            <img width="100%" height="auto" src={config.apiUrl + 'uploads/' + photo}/>
            <h2>{title}</h2>
            <p>
                {`Рейтинг ${(food + interior + service) / count === 0 ? 1 : count}`}
            </p>
            <p>
                <Button onClick={click} bsStyle="primary">Learn more</Button>
            </p>
        </Jumbotron></div>)
};

export default OnePlace;