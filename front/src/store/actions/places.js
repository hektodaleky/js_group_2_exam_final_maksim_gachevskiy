import axios from "../../axios-api";
import {push} from "react-router-redux";
import {PHOTOS_SUCCESS, PLACES_SUCCESS} from "./actionTypes";

const getPlacesSuccess = place => {
    return {
        type: PLACES_SUCCESS, place
    }
};

const photoSuccess = photo => {
    return {
        type: PHOTOS_SUCCESS, photo
    }
};

export const sendNewPlaceData = data => {
    return dispatch => {
        axios.post('/places', data).then(
            response => {
                dispatch(push('/'));
            },
            error => {
                console.log(error.response);
            }
        )
    };
};

export const getPlacePhotos = place =>{
    return dispatch=>{
        axios.post('/places/full',place).then(
            responce=>{
                dispatch(photoSuccess(responce.data));
                // dispatch(push('/'));
                console.log(responce.data)
            }
        )
    }
}

export const getPlaceData = data => {
    return dispatch => {
        axios.get('/places', data).then(
            response => {
                dispatch(getPlacesSuccess(response.data))

            },
            error => {
                console.log(error.response);
            }
        )
    };
};