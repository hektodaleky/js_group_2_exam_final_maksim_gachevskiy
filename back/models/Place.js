const mongoose = require('mongoose');


const Schema = mongoose.Schema;

const PlaceSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    created: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    count: {
        type: Number,
        default: 0,
        required: true
    },
    food: {
        type: Number,
        default: 0
    },
    service: {
        type: Number,
        default: 0
    },
    interior: {
        type: Number,
        default: 0
    },
    photo: {
        type: String,
        required: true
    }
});

const Place = mongoose.model('Place', PlaceSchema);

module.exports = Place;