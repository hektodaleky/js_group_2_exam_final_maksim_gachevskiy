import React, {Component, Fragment} from "react";
import {connect} from 'react-redux';
import {Alert, Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";


import FormElement from "../../componets/UI/Form/FormElement";
import {createNewUser} from "../../store/actions/users";

class Login extends Component {
    state = {
        username: '',
        password: '',
        confirmPassword: '',
        error: null
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        if (this.state.password !== this.state.confirmPassword) {
            this.setState({error: 'Invalid password'});
            return;
        }
        this.props.createNewUser(this.state.username, {first: this.state.password, second: this.state.confirmPassword});
        this.setState({error: null});
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Форма авторизации</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    {this.state.error &&
                    <Alert bsStyle="danger">{this.state.error}</Alert>
                    }
                    {this.props.error &&
                    <Alert bsStyle="danger">{this.props.error.error}</Alert>
                    }

                    <FormElement
                        propertyName="username"
                        title="Имя пользователя"
                        placeholder="Введите имя пользователя"
                        type="text"
                        value={this.state.username}
                        changeHandler={this.inputChangeHandler}
                        autoComplete="current-username"
                    />

                    <FormElement
                        propertyName="password"
                        title="Пароль"
                        placeholder="Введите пароль"
                        type="password"
                        value={this.state.password}
                        changeHandler={this.inputChangeHandler}
                        autoComplete="current-password"
                    />
                    <FormElement
                        propertyName="confirmPassword"
                        title="Пароль еще раз"
                        placeholder="Введите пароль еще раз"
                        type="password"
                        value={this.state.confirmPassword}
                        changeHandler={this.inputChangeHandler}
                        autoComplete="current-password"
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Войти</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
    createNewUser: (user, password) => dispatch(createNewUser(user, password))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);