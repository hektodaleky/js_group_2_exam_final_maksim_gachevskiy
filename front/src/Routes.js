import React from "react";
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import MainMenu from "./containers/MainMenu";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import CreatePlace from "./containers/CreatePlace/CreatePlace";


const ProtectedRoute = ({isAllowed, ...props}) =>
    isAllowed ? <Route {...props} /> : <Redirect to="/login"/>;

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={MainMenu}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/register" exact component={Register}/>
            <ProtectedRoute isAllowed={user} path="/newPlace" exact component={CreatePlace}/>
            <Route render={() => <h1 style={{textAlign: "center"}}>Page not found</h1>}/>
        </Switch>
    );
};

const mapStateToProps = state => {
    return {
        user: state.users.user
    };
};

export default withRouter(connect(mapStateToProps)(Routes));
