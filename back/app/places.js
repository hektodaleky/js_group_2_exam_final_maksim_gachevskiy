const express = require('express');
const nanoid = require('nanoid');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const multer = require('multer');
const config = require('../config');

const path = require('path');
const Place = require('../models/Place');
const Photo = require('../models/Photo');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.post('/', [auth, permit("admin", "user"), upload.single('image')], async (req, res) => {
        const data = req.body;
        data.image = req.file.filename;
        const {title, description} = data;
        const place = new Place({
            title,
            description,
            created: req.user._id,
            photo: data.image
        });

        await place.save();
        console.log({
            user: req.user._id,
            place: place._id,
            name: data.image
        });

        const photo = new Photo({
            user: req.user._id,
            place: place._id,
            name: data.image
        });
        await photo.save()
            .then(resp => res.send(resp))
            .catch(error => res.status(400).send(error))
    });

    router.post('/full', async (req, res) => {
        try {

            const {currentPlace} = req.body;
            const photos = await Photo.find();
            const placePhotos = photos.filter(phot => {
                return phot.place.toString() === currentPlace;
            });
            res.send(placePhotos);
        }
        catch (e) {
            res.status(400).send(e)
        }

    });

    router.get('/', async (req, res) => {
        const places = await Place.find();
        res.send(places);
    });


    return router;
};

module.exports = createRouter;