import React, {Component, Fragment} from 'react';
import FormElement from "../../componets/UI/Form/FormElement";
import {Button, Col, Form, FormControl, FormGroup} from "react-bootstrap";
import {connect} from "react-redux";
import {sendNewPlaceData} from "../../store/actions/places";


class CreatePlace extends Component {
    state = {
        title: '',
        description: '',
        image: '',
        check: false

    };
    sendData = (event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);

        });
        this.props.sendNewPlaceData(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    inputChangeVal = event => {
        this.setState({
            [event.target.name]: event.target.checked
        });
    };


    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            <Fragment>
                <div>
                    <div>
                        <h2>Add new place</h2>
                        <Form horizontal onSubmit={this.sendData}>
                            <FormElement
                                propertyName="title"
                                title="Название"
                                placeholder="Введите название заведения"
                                type="text"
                                value={this.state.title}
                                changeHandler={this.inputChangeHandler}
                            />
                            <FormElement
                                propertyName="description"
                                title="Описание"
                                placeholder="Краткое описание заведения"
                                type="textarea"
                                value={this.state.description}
                                changeHandler={this.inputChangeHandler}
                            />
                            <Col sm={10}>
                                <p style={{fontWeight: 'bold'}}>Изображение</p>
                                <FormControl
                                    type="file"
                                    name="image"
                                    onChange={this.fileChangeHandler}
                                />
                            </Col>
                            <div style={{display: 'flex', width: "100%", padding: "50px"}}>
                                <div style={{width: "30%"}}>
                                    <p style={{fontWeight: 'bold'}}>Соглашение</p><p>Lorem ipsum dolor sit amet,
                                    consectetur adipisicing elit.
                                    Alias aliquam aut doloribus ea, excepturi necessitatibus
                                    omnis reprehenderit suscipit totam vitae!</p></div>

                                <div style={{width: "30%"}}>

                                    <input type="checkbox" value={this.state.check} name="check"
                                           onChange={this.inputChangeVal}/>
                                </div>

                            </div>
                            <FormGroup>
                                <Col smOffset={2} sm={10}>
                                    <Button
                                        bsStyle="primary"
                                        type="submit"
                                    >Отправить</Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    </div>
                </div>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {
        sendNewPlaceData: data => dispatch(sendNewPlaceData(data))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(CreatePlace);
