const express = require('express');

const User = require('../models/User');

const createRouter = () => {
    const router = express.Router();

    router.post('/', (req, res) => {
        const {username, password} = req.body;
        if (password.first !== password.second)
            res.status(400).send({message: "invalid password"});

        const user = new User({
            username,
            password: password.first
        });

        user.save()
            .then(user => res.send(user))
            .catch(error => res.status(400).send(error))
    });


    router.post('/sessions', async (req, res) => {
        let user;
        try {
            user = await User.findOne({username: req.body.username});
        }
        catch (error) {
            res.status(400).send({message: error})
        }

        if (!user) {
            return res.status(400).send({error: 'Имя пользователя или пароль неправильные!'});
        }
        let isMatch;
        try {
            isMatch = await user.checkPassword(req.body.password);
        }
        catch (error) {
            res.status(400).send({message: error})

        }

        if (!isMatch) {
            return res.status(400).send({error: 'Имя пользователя или пароль неправильные!'});
        }

        const token = user.generateToken();
        user.token = token;
        try {
            await user.save();
        }
        catch (error) {
            res.status(400).send({message: error})

        }


        return res.send({message: 'Пользователь и пароль правильные!', user, token});
    });


    router.delete('/sessions', async (req, res) => {
        const token = req.get('Token');
        const success = {message: 'Logout success!'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.token = user.generateToken();
        await user.save();

        return res.send(success);
    });


    return router;
};

module.exports = createRouter;