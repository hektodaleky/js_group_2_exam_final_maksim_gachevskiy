import React, {Component} from 'react';
import {connect} from "react-redux";
import {getPlaceData, getPlacePhotos} from "../store/actions/places";
import OnePlace from "../componets/OnePlace/OnePlace";


class MainMenu extends Component {
    componentDidMount() {
        this.props.getPlaceData();
    }

    render() {
        console.log(this.props.place);
        return (<div style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between'}}>
            {
                this.props.place.map(item => {
                    return (<OnePlace key={item._id} {...item} click={() => this.props.getPlacePhotos(item._id)}/>)
                })
            }
        </div>)
    }
}

const mapStateToProps = state => {
    return {
        place: state.places.places
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getPlaceData: () => dispatch(getPlaceData()),
        getPlacePhotos: place => dispatch(getPlacePhotos(place))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);
